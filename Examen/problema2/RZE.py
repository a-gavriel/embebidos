import queue
import threading
import time


n = [0,1,2,0,0,0,2,3]
n0 = queue.Queue()

# fills the queue with each value of the array
for each in n:
	n0.put(each)
	 

n1 = queue.Queue()
n2 = queue.Queue()

a0 = queue.Queue()
a1 = queue.Queue()
a2 = queue.Queue()

b = queue.Queue()

c0 = queue.Queue()
c1 = queue.Queue()
c2 = queue.Queue()

d = queue.Queue()
e = queue.Queue()

d.put(0)

 


def iszero(a,out):
	while True:        
		time.sleep(1)
		x = a.get()        
		result = int(not (bool(x)))
		out.put(result)

def add(a,b,out):
  while True:
      x = a.get()
      y = b.get()             
      out.put(x+y)

def split2(inp,out1, out2):
	while True:
		x = inp.get()		
		out1.put(x)
		out2.put(x)

def ifpass(a,cond,out):
	while True:
		x = a.get()
		y = cond.get()
		result = 0
		if y == 1:
			result = x		
		out.put(result)

def join(n,counter,out):
	x = 0
	y = 0
	while True:
		x = y
		y = counter.get()
		val = n.get()
		if y == 0:
			out.put(x)
			out.put(val)		

def delay(inp,out):
	while True:
		x = inp.get()		
		out.put(x)

def result(inp):
	while True:
		x = inp.get()
		print(x)


splitN = threading.Thread(target=split2, args=(n0,n1,n2) )
compare = threading.Thread(target=iszero, args=(n1,a0) )
splitA = threading.Thread(target=split2, args=(a0,a1,a2) )
fadd = threading.Thread(target=add, args=(a2,d,b) )

fifpass = threading.Thread(target=ifpass, args=(b,a1,c0) )
splitC = threading.Thread(target=split2, args=(c0,c1,c2) )

fdelay0 = threading.Thread(target=delay, args=(c2,d) )

fjoin = threading.Thread(target=join, args=(n2,c1,e) )

fresult = threading.Thread(target=result, args=(e,) )


splitN.start()
compare.start()
splitA.start()
fadd.start()
fifpass.start()
splitC.start()
fdelay0.start()
fjoin.start()
fresult.start()

