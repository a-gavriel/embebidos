
mkdir Ej3
cd Ej3
#head -c 1KB /dev/zero > empty.img
truncate -s 1K empty.img
#fallocate -l 1024 empty.img
#echo  > empty.img
chmod +x empty.img
echo clear >> empty.img
echo "wget http://www.bolis.com/onyx/random/stuff/sounds/murray/murrays.wav" >> empty.img
echo "aplay murrays.wav" >> empty.img
echo "mv murrays.wav .murrays.wav" >> empty.img
echo "echo Aquı́ no ha pasado nada..." >> empty.img
mv ./empty.img ./script.x
sh script.x

#Ejercicio 3 pregunta: debería ejecutarse el archivo creado, pero este no se ejecuta debido a la forma en que se generó el archivo. Para poder ejecutar el archivo se debe correr con la consola: "sh" . El script creado primero descarga un archivo de sonido llamado murrays.wav, luego reproduce el archivo y finalmente renombra el archivo agregándole un " . " al inicio, lo que provoca que el archivo adquiera el atributo de "oculto".
