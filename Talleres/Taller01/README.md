#Introducción a los sistemas Embebidos
##Taller 1

Se recomienda borrar todos los archivos y directorios exceptuando *.sh README.md investigacion.txt

Para dar permiso de ejecución a cada uno de los scripts:

```
chmod +x Ejercicio1.sh Ejercicio2.sh Ejercicio3.sh Ejercicio4.sh
```

Para ejecutarlos:
```
./Ejercicio1.sh
./Ejercicio2.sh
./Ejercicio3.sh
./Ejercicio4.sh
```
