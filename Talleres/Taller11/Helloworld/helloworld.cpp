//file:helloworld.cpp
//Allsystemcmodulesshouldincludesystemc.hheaderfile
#include "systemc.h"
//Helloworldismodulename
SC_MODULE(hello_world){
  SC_CTOR(hello_world){
    //Nothinginconstructor
  }
  //c++likemethod
  void be_nice(){
  //Print”HelloWorld”totheconsole.
  cout << "HelloWorldfromsystemc.\n";
}
};
//scmainintoplevelfunctionlikeinC++main
int sc_main(int argc,char* argv[]){
  //helloworld”object”
  hello_world hello("NULL");
  //Printthehelloworld
  hello.be_nice();
  return(0);
}