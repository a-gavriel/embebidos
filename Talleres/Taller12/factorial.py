import queue
import threading
import time

a = queue.Queue()
b = queue.Queue()
c = queue.Queue()
d = queue.Queue()
e = queue.Queue()
f = queue.Queue()
g = queue.Queue()
h = queue.Queue()
c.put(0)
g.put(1)

def add(a,out):
    while True:
        x = a.get()        
        time.sleep(1)
        out.put(x+1)

def split2(inp,out1, out2):
    while True:
        x = inp.get()
        out1.put(x)
        out2.put(x)        

def mult(a,b,out):
    while True:
        x = a.get()
        y = b.get()
        out.put(x*y)

def delay(inp,out):
    while True:
        x = inp.get()
        out.put(x)

def result(inp):
    while True:
        x = inp.get()
        print(x)

fadd = threading.Thread(target=add, args=(c,a) )
fsplit1 = threading.Thread(target=split2, args=(a,b,d) )
fdelay0 = threading.Thread(target=delay, args=(b,c) )

fmul = threading.Thread(target=mult, args=(d,g,e) )
fsplit2 = threading.Thread(target=split2, args=(e,f,h) )
fdelay1 = threading.Thread(target=delay, args=(f,g) )
fresult = threading.Thread(target=result, args=(h,) )


fadd.start()
fsplit1.start()
fdelay0.start()

fmul.start()
fsplit2.start()
fdelay1.start()
fresult.start()

