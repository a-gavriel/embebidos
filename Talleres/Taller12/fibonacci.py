import queue
import threading
import time

a = queue.Queue()
b = queue.Queue()
b.put(1)
c = queue.Queue()
d = queue.Queue()
e = queue.Queue()
f = queue.Queue()
f.put(0)

def add(a,b,out):
    while True:
        x = a.get()
        y = b.get()
        time.sleep(1)
        out.put(x+y)

def split(inp,out1, out2, out3):
    while True:
        x = inp.get()
        out1.put(x)
        out2.put(x)
        out3.put(x)

def delay(inp,out):
    while True:
        x = inp.get()
        out.put(x)

def result(inp):
    while True:
        x = inp.get()
        print(x)

fadd = threading.Thread(target=add, args=(c,f,a) )
fdelay1 = threading.Thread(target=delay, args=(a,b) )
fdelay0 = threading.Thread(target=delay, args=(e,f) )
fsplit = threading.Thread(target=split, args=(b,c,d,e) )
fresult = threading.Thread(target=result, args=(d,) )


fadd.start()
fdelay1.start()
fdelay0.start()
fsplit.start()
fresult.start()

