#ifndef LIBGPIO_H
#define LIBGPIO_H

void exportPin(char pin);

int testPinErr(char pin);

void pinMode(char pin, char *direction);

void digitalWrite(char pin, char value);

int digitalRead(char pin);

void blink(char pin, float freq, float duration);

#endif