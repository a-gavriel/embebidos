autoreconf --verbose --force --install || {
 echo 'autogen.sh failed';
 exit 1;
}

echo
echo "Now run './configure' with your system settings followed by 'make' to compile this module."
echo