Introducción a los Sistemas Embebidos
Taller 7
Alexis Gavriel Gómez
2016085662


1. ¿Qué pasos debe seguir antes de escribir o leer de un puerto de entrada/salida general (GPIO)?
Para que el pin esté disponible en el sistema de archivos se debe exportar el pin. 
Esto se realiza ejecutando el comando:
	echo <pinNumber> > /sys/class/gpio/export
Se debe tener cuidado de no intentar exportar un pin que ya ha sido exportado previamente.

* Se debe seleccionar la dirección del pin (entrada o salida)
	echo "direction" > /sys/class/gpio/gpio<pinNumber>/"direction"
	en donde se reemplaza "direction" con "in" o "out"

* Para leer o escribir en el pin
	Escribir:
		echo "value" > /sys/class/gpio/gpio<pinNumber>/value
		Donde value puede ser 1 o 0
	Leer:
		cat /sys/class/gpio/gpio<pinNumber>/value


2. ¿Que comando podría utilizar, bajo Linux, para escribir a un GPIO específico?
	echo "value" > /sys/class/gpio/gpio<pinNumber>/value
	Donde value puede ser 1 o 0
