#include <stdlib.h>
#include <stdio.h>
#include <time.h>


char command[512] = {};

void exportPin(char pin){
	sprintf(command, "echo \"%d\" > /sys/class/gpio/export", pin);
	system(command);
}

int testPinErr(char pin){
	sprintf(command, "echo /sys/class/gpio/gpio%d/", pin);
	return system(command);
}

void pinMode(char pin, char *direction){
	if(testPinErr(pin))exportPin(pin);
	sprintf(command, "echo \"%s\" > /sys/class/gpio/gpio%d/direction", direction, pin);
	system(command);
}

void digitalWrite(char pin, char value){
	if(testPinErr(pin))exportPin(pin);
	sprintf(command, "echo \"%d\" > /sys/class/gpio/gpio%d/value", value, pin);
	system(command);	
}

int digitalRead(char pin){
	int value;
	sprintf(command, "/sys/class/gpio/gpio%d/value", pin);
	FILE* ptr = fopen(command,"r");
	fscanf(ptr,"%d", value);
	fclose(ptr);
	return value;
}

void blink(char pin, float freq, float duration){
	clock_t startTime = clock();
	char value = 0;
	float period = 1.0/freq;
	float lastPeriod = (float)clock()/1000.0;
	while((clock()-startTime)<duration){
		float newPeriod = (float)clock()/1000.0;
		if((newPeriod-lastPeriod) >= period){
			digitalWrite(pin, value);
			value = (value+1)%2;
			lastPeriod = newPeriod;
		}
	}
}