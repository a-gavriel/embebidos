#include <libgpio.h>

int main(){

	pinMode(1, "in");
	pinMode(2, "out");
	pinMode(3, "out");

	digitalWrite(2, 1);
	blink(3, 100, 6);

	printf("Input value on pin = %d\n", digitalRead(1));
	
	return 0;
}