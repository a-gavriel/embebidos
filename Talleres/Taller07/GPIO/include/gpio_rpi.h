#ifndef _GPIO_RPI_H
#define _GPIO_RPI_H

int exportPin(int pin);
int pinMode(int pin, int MODE);
int digitalWrite(int pin, int value);
int digitalRead(int pin);
int blink(int pin, int freq, int duration);

#endif
