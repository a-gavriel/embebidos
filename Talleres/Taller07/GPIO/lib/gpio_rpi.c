#include <stdlib.h>
#include <stdio.h>

#include "../include/gpio_rpi.h"


int exportPin(int pin){
  char command [128] = "";
  sprintf(command, "echo '%d' > /sys/class/gpio/export", pin);
  return system(command);
}

int pinMode(int pin, int MODE){  
  char command [128] = "";
  if (MODE){
    sprintf(command, "echo 'out' > /sys/class/gpio/gpio%d/direction", pin);
  }else{
    sprintf(command, "echo 'in' > /sys/class/gpio/gpio%d/direction", pin);
  }
  return system(command);
}

int digitalWrite(int pin, int value){
  char command [128] = "";
  if (value){
    sprintf(command, "echo '1' > /sys/class/gpio/gpio%d/value", pin);
  }else{
    sprintf(command, "echo '0' > /sys/class/gpio/gpio%d/value", pin);
  }
  return system(command);
}

int digitalRead(int pin){
  char command [128] = "";
  return 1;
}

int blink(int pin, int freq, int duration){
  char command [128] = "";
  return 1;
}

int main(){
  exportPin(1);
}
