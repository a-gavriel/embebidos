#Introducción a los sistemas Embebidos
##Taller 2

Para eliminar los archivos compilados:
```
make clean
```

Para dar permiso de ejecución al script

```
chmod +x Taller2.sh
```

Para ejecutarlo:
```
./Taller2.sh
```
