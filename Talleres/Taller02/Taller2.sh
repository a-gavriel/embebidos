  echo -e "\n-------------------------\nCleaning\n-------------------------\n"	
  rm -f libbiblioteca.so 
	rm -f *.o
	rm -f ejecutable
	rm -f ejecutable_s
	rm -f ejecutable_d
	rm -f libbiblioteca.a
  
  echo -e "\n-------------------------\nCompiling Static\n-------------------------\n"
  gcc -c biblioteca.c
	ar rv libbiblioteca.a biblioteca.o
	ranlib libbiblioteca.a
	nm -s libbiblioteca.a
	gcc -o ejecutable_s calculadora.c libbiblioteca.a -lm

  echo -e "\n-------------------------\nCompiling Dynamic\n-------------------------\n"
  gcc -c biblioteca.c
	ld -o libbiblioteca.so biblioteca.o -shared
	gcc -o ejecutable_d calculadora.c -Bdynamic libbiblioteca.so -lm		
	cwd=`pwd` && 	export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:${cwd}

  echo -e "\n-------------------------\nRunning Static\n-------------------------\n"
  ./ejecutable_s
  echo -e "\n-------------------------\nRunning Dynamic\n-------------------------\n"
  ./ejecutable_d

